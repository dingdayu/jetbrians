# jetbrians
Docker jetbrians IntelliJIDEALicenseServer。

[中文请点击](README_cn.md)

### Example

Please make sure docker is installed!

```
docker run -p 1128:1128 dingdayu/jetbrians
```

more please visit: [AT_DOCKER](docs/AT_DOCKER.md)

### Activation

Activate new License with: 

- [ ] JetBrains Account
- [ ] Activation code
- [x] License server

License server address:

```
http://host:1128
```

### Nginx

```
server {
        listen       80;
        server_name ide.example.com;
        error_log  logs/ide.error.log;
        access_log  logs/ide.access.log  main;

        location / {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_pass http://127.0.0.1:1128;
            proxy_redirect http://127.0.0.1:1128 /;
        }
}
```

File: [nginx.conf](nginx.conf)

### Statement

1. Do not use it illegally.
2. Please support genuine.
3. If the infringement please contact remove.

### Support

- IntelliJ IDEA>=7.0
- ReSharper>=3.1
- ReSharper>=Cpp 1.0
- dotTrace>=5.5
- dotMemory>=4.0
- dotCover>=1.0
- RubyMine>=1.0
- PyCharm>=1.0
- WebStorm>=1.0
- PhpStorm>=1.0
- AppCode>=1.0
- CLion>=1.0

### Link

1. [IntelliJ IDEA License Server v1.5](http://blog.lanyus.com/archives/314.html)

### Sponsor

<p align="center">
    <img width="400" src="https://github.com/dingdayu/jetbrians/blob/master/images/merge.png"></img>
</p>
